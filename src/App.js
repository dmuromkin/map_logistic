import React, { Component } from "react";
import OlMap from "ol/Map";
import OlView from "ol/View";
import TileLayer from "ol/layer/Tile";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import Image from "ol/layer/VectorImage"
import OSM from "ol/source/OSM";
import { List, Modal, Divider, Button } from "antd";
import MousePosition from "ol/control/MousePosition";
import { createStringXY, format } from "ol/coordinate";
import { defaults as defaultControls } from "ol/control";
import { connect } from "react-redux";
import { Layout } from "antd";
import "./App.css";
import "ol/ol.css";
import "antd/dist/antd.css";
import { MapComponent } from "@terrestris/react-geo";
import MyTable from "./layersTable";
import RightPanel from "./ParametrPanel";
import XYZ from "ol/source/XYZ";
import NetworkLine from "./NetworkLine.png";
import HeatLine from "./HeatLine.png";
import PipeLine from "./PipeLine.png";
import SewerageLine from "./SewerageLine.png";
import axios from "axios";
import { Feature } from "ol";

const { Sider, Content } = Layout;
let layerName,
  objectName = [],
  id,
  layerImg,
  i;
var layerTranslations = [
  "Водопровод",
  "Канализация",
  "Тепловая сеть",
  "Электрическая сеть",
];
let objects = [];
let mousePositionControl = new MousePosition({
  coordinateFormat: createStringXY(4),
  projection: "EPSG:4326",
});

const center = [4181375.872313928, 7514318.29293211];

let extent = [
  4098520.106899999,
  7392801.4089,
  4225527.205200002,
  7561700.2141999975,
];

var layers = [
  new TileLayer({
    source: new OSM(),
  }),
];
var urls = [
  "https://teplo.ispu.ru/arcgis/rest/services/test/Rkii_arcgis_dev_water_tile/MapServer",
  "https://teplo.ispu.ru/arcgis/rest/services/test/Rkii_arcgis_dev_sewerage_tile/MapServer",
  "https://teplo.ispu.ru/arcgis/rest/services/test/Rkii_arcgis_dev_heat_tile/MapServer",
  "https://teplo.ispu.ru/arcgis/rest/services/test/Rkii_arcgis_dev_energy_tile/MapServer",
];

for (i = 0; i < urls.length; ++i) {
  layers.push(
    new TileLayer({
      visible: true,
      extent: extent,
      source: new XYZ({
        url: urls[i] + "/tile/{z}/{y}/{x}",
      }),
    })
  );
}

const map = new OlMap({
  controls: defaultControls().extend([mousePositionControl]),
  view: new OlView({
    center: center,
    zoom: 15,
  }),
  layers: layers,
});

function Identify(store, evt) {
  store.onClear();
  let x = evt.coordinate[0];
  let y = evt.coordinate[1];
  let extent = [x - 4, y - 4, x + 4, y + 4];
  let layers = [
    "Rkii_arcgis_dev_energy_tile",
    "Rkii_arcgis_dev_heat_tile",
    "Rkii_arcgis_dev_water_tile",
    "Rkii_arcgis_dev_sewerage_tile",
  ];
  let icons = [NetworkLine, HeatLine, PipeLine, SewerageLine];

  // eslint-disable-next-line
  layers.map((layer) => {
    var url =
      "https://teplo.ispu.ru/arcgis/rest/services/test/" +
      layer +
      "/MapServer/identify?geometry=" +
      x +
      "%2C" +
      y +
      "&geometryType=esriGeometryPoint&sr=&layers=&layerDefs=&time=&layerTimeOptions=&tolerance=1&mapExtent=" +
      extent[0] +
      "%2C" +
      extent[1] +
      "%2C" +
      extent[2] +
      "%2C" +
      extent[3] +
      "&imageDisplay=7%2C7%2C96&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&dynamicLayers=&returnZ=false&returnM=false&gdbVersion=&f=pjson";

    fetch(url)
      .then((response) => response.json())
      .then((response) => {
        if (response.results.length > 0) {
          objectName = [];
          layerName = response.results[0].layerName;
          id = response.results[0].layerId;
          if (layerName === "Pipeline" && id === 3) layerName = "Sewerage";
          layerImg = icons[layers.indexOf(layer)];
          for (let i = 0; i < response.results.length; i++)
            objectName.push(response.results[i]);
          let newpoint = [layerName, layerImg, objectName];
          store.onAddPoint(newpoint);
        }
      });
  });
}


function changeVis(name) {
  var index = layerTranslations.indexOf(name) + 1;
  let curState = layers[index].getVisible();
  layers[index].setVisible(!curState);
}
function open(isVis) {
  isVis.setState({ vis: !isVis.state.vis });
}
let thisObj = [];
let isopen;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vis: false,
    };
  }

  render() {
    let store = this.props;
    let isvis = this;

    map.once("click", function (evt) {
      isopen = false;
      Identify(store, evt);
      setTimeout(() => {
        open(isvis);
      }, 150);
      evt.preventDefault();
    });

    function Show(object) {
      isopen = true;
      thisObj = [];
      open(isvis);
      thisObj = object;
    }

    return (
      <div className="App">
        <Layout>
          <Sider width="160" theme="light">
            <Divider orientation="left">Слои</Divider>
            <List
              size="small"
              bordered
              dataSource={layerTranslations}
              renderItem={(item) => (
                <List.Item onClick={() => changeVis(item)}>{item}</List.Item>
              )}
            />
            
          </Sider>
          <Content>
            <Modal
              width="50%"
              visible={this.state.vis}
              onOk={() => this.setState({ vis: !this.state.vis })}
              onCancel={() => this.setState({ vis: !this.state.vis })}
            >
              <MyTable store={store} onClicks={Show} />
            </Modal>
            <div className="mapField">
              <MapComponent map={map} />
            </div>
          </Content>
          <RightPanel store={store} object={thisObj} open={isopen} />
        </Layout>
      </div>
    );
  }
}
export default connect(
  (state) => ({
    currState: state,
  }),
  (dispatch) => ({
    onAddPoint: (newpoint) => {
      dispatch({ type: "ADD_POINT", payload: newpoint });
    },
    onClear: () => {
      dispatch({ type: "USER_LOGOUT" });
    },
  })
)(App);

//
/*
function FindConnections() {
  var url= "https://85.142.148.146:6443/arcgis/rest/services/RKII/rkii_heat/MapServer/11/query?where=objectid+in+%2818298%2C+18304%2C+18314%2C+18483%2C+21156%2C+21165%29&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&f=pjson"
  fetch(url)
      .then((response) => response.json())
      .then((response) => {
          console.log(response.features[0].geometry)
          const vectorLayer = new Image(
            {
              source: new VectorSource({
                features: response.features
              })
            }
          )
          map.addLayer(vectorLayer)
        })
<Button onClick={()=> FindConnections()}>click me</Button>
}*/