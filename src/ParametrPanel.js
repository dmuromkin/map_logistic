import React, { useState } from "react";
import { Menu, Layout, Input, Button } from "antd";
const { SubMenu } = Menu;
const { Sider } = Layout;
const { TextArea } = Input;

function RightPanel(props) {
  function change(all) {
    update((prev) => all);
  }

  const [currObject, update] = useState([]);

  if (currObject !== props.object) {
    change(props.object);
  }
  if (props.store.currState.length !== 0 && props.open !== false && currObject === props.object) {
    return (
      <Sider width={200}>
        <Menu
          style={{ width: 200, height: "100%" }}
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
        >
          <SubMenu key="sub1" title="Обьект">
            <div style={{ marginTop: "1rem", fontSize: "10px" }}>
              Наименование
            </div>
            <div style={{ marginTop: "1rem" }} />
            <TextArea defaultValue={currObject.attributes.name} autoSize allowClear />
            <div style={{ margin: "24px 0" }} />
            <div style={{ marginTop: "1rem", fontSize: "10px" }}>ID</div>
            <div style={{ marginTop: "1rem" }} />
            <TextArea defaultValue={currObject.attributes.id} autoSize allowClear />
            <div style={{ margin: "24px 0" }} />
            <div style={{ marginTop: "1rem", fontSize: "10px" }}>ObjectID</div>
            <div style={{ marginTop: "1rem" }} />
            <TextArea defaultValue={currObject.attributes.objectid} autoSize allowClear />
            <div style={{ margin: "24px 0" }} />
          </SubMenu>
          <Button
            onClick={() => {
              props.store.onClear();
            }}
          >
            Закрыть панель
          </Button>
        </Menu>
      </Sider>
    );
  } else {
    return null;
  }
}

export default RightPanel;
