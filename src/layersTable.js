import React from "react";
import { Row, Col, Divider } from "antd";

function MyTable(props) {
  if (props.store.currState.length === 0) {
    return (
      <div>
        <Divider orientation="left">Информация</Divider>

        <Row justify="left">
          <Col className="gutter-row" span={12}>
            <div>Информация недоступна</div>
          </Col>
        </Row>
      </div>
    );
  } else {
    return (
      <div>
        <Divider orientation="left">Информация</Divider>
        {props.store.currState.map((layer, index) => (
          <div key={index}>
            <Row className="gutter-row">
              <div style={{ fontWeight: "bold" }}>{layer[0]}</div>
              <img src={layer[1]} alt="" />
            </Row>
            {layer[2].map((object, id) => (
              <Row key={id} className="gutter-row">
                <div onClick={()=> props.onClicks(object)}>
                  Обьект {id + 1}: {object.attributes.name}
                </div>
              </Row>
            ))}
          </div>
        ))}
      </div>
    );
  }
}
export default MyTable;